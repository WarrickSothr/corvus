from setuptools import setup

setup(
    name='corvus',
    packages=['corvus'],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
)