SET PIPENV_VERBOSITY=-1
SET PYTHONPATH=%cd%

pipenv run python3 --version
pipenv run python3 -m pip --version

pipenv run pylint --version
pipenv run mypy --version
pipenv run coverage --version
pipenv run pytest --version
pipenv run pycodestyle --version
pipenv run pydocstyle --version

pipenv run pylint corvus
if %errorlevel% neq 0 exit /b %errorlevel%

pipenv run mypy corvus tests
if %errorlevel% neq 0 exit /b %errorlevel%

pipenv run coverage run --source corvus -m pytest
if %errorlevel% neq 0 exit /b %errorlevel%

pipenv run coverage report --fail-under=85 -m --skip-covered
if %errorlevel% neq 0 exit /b %errorlevel%

pipenv run pycodestyle corvus tests
if %errorlevel% neq 0 exit /b %errorlevel%

pipenv run pydocstyle corvus
if %errorlevel% neq 0 exit /b %errorlevel%
