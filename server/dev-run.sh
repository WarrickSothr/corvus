#!/usr/bin/env bash

FLASK_APP=corvus:corvus flask db upgrade
python manage.py user register-admin
FLASK_APP=corvus:corvus flask run
