#!/usr/bin/env bash

set -e
set -x

# shellcheck disable=SC2034
PIPENV_VERBOSITY=-1

pipenv run python3 --version
pipenv run python3 -m pip --version

pipenv run pylint --version
pipenv run mypy --version
pipenv run coverage --version
pipenv run pytest --version
pipenv run pycodestyle --version
pipenv run pydocstyle --version

pipenv run pylint corvus
pipenv run mypy corvus tests
PYTHONPATH=$(pwd) pipenv run coverage run --source corvus -m pytest
pipenv run coverage report --fail-under=85 -m --skip-covered
pipenv run pycodestyle corvus tests
pipenv run pydocstyle corvus
