.. Corvus documentation master file, created by
   sphinx-quickstart on Sun Jul 29 11:09:44 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Corvus's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   api/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
