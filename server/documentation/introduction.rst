Introduction To Corvus
========================

Corvus is the scaffolding for python web services. Among the things it offers are the following:

* Management CLI for working with users
* Prebuilt auth framework with roles and tokens
* Hierarchical role models
* Stong testing foundation
* Strong code formatting testing foundation
* PipEnv scaffold for ensuring reproducable builds
* Prebuilt docker environments for runtime
* Pythonenv integration
* Lightweight Flask backend
