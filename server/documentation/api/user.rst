User API
========

.. http:get:: /user

    Get a page of users.

    **Example request**

    .. sourcecode:: http

        GET /user HTTP/1.1
        Host: example.tld
        Accept: application/json
        X-Auth-Token: <Base64(user:userToken)>

    **Example response**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: application/json

        {
            "page": 1,
            "count": 1,
            "totalCount": 1,
            "lastPage": 1,
            "items": [
                {
                    "creationTime": "2018-07-29T11:58:17-05:00",
                    "lastLoginTime": "2018-07-29T12:43:27-05:00",
                    "name": "corvus_administrator",
                    "role": "ADMIN",
                    "version": 0
                }
            ]
        }

    :query int page: User page to retrieve
    :query int perPage: Number of records to retrieve per page (max 100)
    :<header Accept: Response content type depends on :mailheader:`Accept` header
    :<header X-Auth-Token: Encoded token authorization
    :>header Content-Type: Depends on :mailheader:`Accept` header of request
    :>json int page: Page retrieved
    :>json int count: Number of items returned
    :>json int totalCount: Total number of items available
    :>json int lastPage: Last page that can be requested before 404
    :>json int items: List of Users
    :statuscode 200: Successfully retrieved the user
    :statuscode 400: Invalid page or perPage values
    :statuscode 401: Authorization failed
    :statuscode 404: User page doesn't exist


.. http:get:: /user/(str:user_name)

    Find a user by name.

    **Example request**:

    .. sourcecode:: http

        GET /user/corvus_administrator HTTP/1.1
        Host: example.tld
        Accept: application/json
        X-Auth-Token: <Base64(user:userToken)>

    **Example response**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: application/json

        {
            "creationTime": "2018-07-29T11:58:17-05:00",
            "lastLoginTime": "2018-07-29T12:43:27-05:00",
            "name": "corvus_administrator",
            "role": "ADMIN",
            "version": 0
        }

    :param string user_name: Name of the user to retrieve information about
    :<header Accept: Response content type depends on :mailheader:`Accept` header
    :<header X-Auth-Token: Encoded token authorization
    :>header Content-Type: Depends on :mailheader:`Accept` header of request
    :>json datetime creationTime: Creation time for the user
    :>json datetime lastLoginTime: When the user last logged in, or was last bumped
    :>json string name: The user name
    :>json string role: The role assigned to the user
    :>json int version: Version information
    :statuscode 200: Successfully retrieved the user
    :statuscode 401: Authorization failed
    :statuscode 404: User doesn't exist

.. http:patch:: /user/(str:user_name)

    Patch a user.

    **Example request**:

    .. sourcecode:: http

        PATCH /user/corvus_administrator HTTP/1.1
        Host: example.tld
        Accept: application/json
        X-Auth-Token: <Base64(user:userToken)>
        Content-Type: application/json

        {
            "lastLoginTime": "2019-07-29T12:43:27-05:00",
            "version": 0
        }

    **Example response**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: application/json

        {
            "creationTime": "2018-07-29T11:58:17-05:00",
            "lastLoginTime": "2019-07-29T12:43:27-05:00",
            "name": "corvus_administrator",
            "role": "ADMIN",
            "version": 1
        }

    :param string user_name: Name of the user to update
    :<header Accept: Response content type depends on :mailheader:`Accept` header
    :<header X-Auth-Token: Encoded token authorization
    :<header Content-Type: application/json
    :<json datetime createDateTime: Update createDateTime (Administrator Only)
    :<json datetime lastLoginTime: Update lastLoginTime
    :<json string name: Update user name (Administrator Only)
    :<json string role: Update user role (Must be less than or equal to the role authenticating the action)
    :<json int version: Must match the latest version of the user
    :>header Content-Type: Depends on :mailheader:`Accept` header of request
    :>json datetime creationTime: Creation time for the user
    :>json datetime lastLoginTime: When the user last logged in, or was last bumped
    :>json string name: The user name
    :>json string role: The role assigned to the user
    :>json int version: Version information
    :statuscode 200: Successfully patched the user
    :statuscode 400: An issue in the payload was discovered
    :statuscode 401: Authorization failed
    :statuscode 404: User doesn't exist

.. http:post:: /user

    Register a new user with the service.

    **Example request**:

    .. sourcecode:: http

        POST /user HTTP/1.1
        Host: example.tld
        Accept: application/json
        X-Auth-Token: <Base64(user:userToken)>
        Content-Type: application/json

        {
            "name": "test_user",
            "password": "JvZ9bm79",
            "role": "USER"
        }

    **Example response**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: application/json

        {
            "creationTime": "2018-07-29T14:16:48-05:00",
            "name": "test_user",
            "role": "USER",
            "version": 0
        }

    :<header Accept: Response content type depends on :mailheader:`Accept` header
    :<header X-Auth-Token: Encoded token authorization
    :<header Content-Type: application/json
    :<json string name: Name of the user
    :<json string password: Password to use
    :<json string role: Role to assign to the user (Must be less than or equal to the role of the authenticating user)
    :>header Content-Type: Depends on :mailheader:`Accept` header of request
    :>json datetime creationTime: Datetime the user was created
    :>json string name: Name of the created user
    :>json string role: Role of the created user
    :>json int version: Version number of the created user
    :statuscode 200: Successfully registered the user
    :statuscode 400: An issue in the payload was discovered
    :statuscode 401: Authorization failed

.. http:delete:: /user/(str:user_name)

    Register a new user with the service.

    **Example request**:

    .. sourcecode:: http

        DELETE /user/test_user HTTP/1.1
        Host: example.tld
        Accept: application/json
        X-Auth-Token: <Base64(user:userToken)>

    **Example response**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: application/json

        {
            "message": "Successfully Deleted",
            "success": true
        }

    :param string user_name: Name of the user to delete
    :<header Accept: Response content type depends on :mailheader:`Accept` header
    :<header X-Auth-Token: Encoded token authorization
    :>header Content-Type: Depends on :mailheader:`Accept` header of request
    :>json string message: Success or failure message
    :>json boolean success: Action status indicator
    :statuscode 200: Successfully deleted the user
    :statuscode 401: Authorization failed
    :statuscode 404: User doesn't exist

User Object Models
==================

.. json:object:: Page<User>
    :showexample:

    Page<User> definition

    :property page: The page returned
    :proptype page: integer
    :property count: The number of items on this page
    :proptype count: integer
    :property totalCount: The total number of items available
    :proptype totalCount: integer
    :property lastPage: The last page that is accessible before 404
    :proptype lastPage: integer
    :property items: The list of items on the page
    :proptype items: :json:list: 'User'

.. json:object:: User
    :showexample:

    User definition

    :property name: The unique name of the user
    :proptype name: string
    :property creationTime: The time that the user was created
    :proptype creationTime: iso8601
    :property lastLoginTime: The time that the user last logged in
    :proptype lastLoginTime: iso8601
    :property version: An identifier for the user version
    :proptype version: integer
    :property role: The assigned role for the user
    :proptype role: string
