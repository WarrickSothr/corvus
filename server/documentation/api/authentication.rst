Authentication API
==================

.. http:post:: /auth/login

    Authenticate with the server and receive a userToken for requests.

    **Example request**:

    .. sourcecode:: http

        POST /auth/login HTTP/1.1
        Host: example.tld
        Accept: application/json
        Authorization: Basic <Base64 Encoded Basic Auth>

    **Example response**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: application/json

        {
            "creationTime": "2018-07-29T11:59:29-05:00",
            "enabled": true,
            "token": "b94cf5c7-cddc-4610-9d4c-6b8e04088ae8",
            "version": 0
        }

    :<header Accept: Response content type depends on :mailheader:`Accept` header
    :<header Authorization: Encoded basic authorization
    :>header Content-Type: Depends on :mailheader:`Accept` header of request
    :>json datetime creationTime: Creation time for the userToken
    :>json datetime expirationTime: Expiration time for the userToken
    :>json boolean enabled: Whether the userToken is enabled
    :>json string token: UserToken to use for further authentication
    :>json int version: Version for the object
    :statuscode 200: User successfully logged in
    :statuscode 401: Authorization failed

.. http:post:: /auth/bump

    Bump user login information.

    **Example request**:

    .. sourcecode:: http

        POST /auth/bump HTTP/1.1
        Host: example.tld
        Accept: application/json
        X-Auth-Token: <Base64(user:userToken)>

    **Example response**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: application/json

        {
            "lastLoginTime": "2018-07-29T12:15:51-05:00"
        }

    :<header Accept: Response content type depends on :mailheader:`Accept` header
    :<header X-Auth-Token: Encoded token authorization
    :>header Content-Type: Depends on :mailheader:`Accept` header of request
    :>json datetime lastLoginTime: Updated lastLoginTime for the user
    :statuscode 200: User last_login_time successfully bumped
    :statuscode 401: Authorization failed

.. http:post:: /auth/logout

    Logout a user and remove the provided userToken from valid tokens.

    **Example request**:

    .. sourcecode:: http

      POST /auth/logout HTTP/1.1
      Host: example.tld
      Accept: application/json
      X-Auth-Token: <Base64(user:userToken)>

    **Example response**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: application/json

        {
            "success": true
        }

    :<header Accept: Response content type depends on :mailheader:`Accept` header
    :<header X-Auth-Token: Encoded token authorization
    :>header Content-Type: Depends on :mailheader:`Accept` header of request
    :>json boolean success: Whether the logout was successful
    :statuscode 200: User successfully logged out
    :statuscode 401: Authorization failed

Authentication Object Models
============================

.. json:object:: UserToken
   :showexample:

   UserToken definition

   :property token: The token value, used for authentication
   :proptype token: string
   :property note: Additional information about the token
   :proptype note: string
   :property enabled: Determine if a token will be accepted
   :proptype enabled: boolean
   :property expirationTime: The time that the token becomes invalid, regardless of enabled state
   :proptype expirationTime: iso8601
   :property creationTime: The time that the token was created
   :proptype creationTime: iso8601
   :property lastUsageTime: The time that the token was last used
   :proptype lastUsageTime: iso8601
   :property version: An identifier for the token version
   :proptype version: integer
