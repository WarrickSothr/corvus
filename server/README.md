# Corvus Server

## [API Documentation](https://warricksothr.gitlab.io/Corvus)

## Requirements

* Python 3.6
* Pipenv

## Installation

```bash
git clone https://gitlab.com/WarrickSothr/Corvus.git
cd Corvus/server
pipenv install
pipenv shell
```

## Configuration

## Running

### Docker

```bash
docker build -t corvus:local-test .
docker run -d corvus:local-test
```

### Local Development Version

```bash
FLASK_APP=corvus:corvus flask db upgrade
python manage.py user register-admin
FLASK_APP=corvus:corvus flask run
```

## FAQ

## Development

```bash
pipenv install --dev
```

* Make changes
* Add/Update tests

```bash
./run_tests
```

* If everything passes follow contributing guide.

## Contributing

See ../CONTRIBUTING.md