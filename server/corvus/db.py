"""Database configuration and methods."""

from flask_migrate import upgrade
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.ext.declarative import DeclarativeMeta

db: SQLAlchemy = SQLAlchemy()
db_model: DeclarativeMeta = db.Model


def init_db() -> None:
    """Clear existing data and create new tables."""
    upgrade('migrations')
