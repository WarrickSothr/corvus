"""User related SQLALchemy models."""
from sqlalchemy import Enum

from corvus.db import db
from corvus.service.role_service import Role


class User(db.Model):  # pylint: disable=too-few-public-methods
    """Represents a user in the system."""

    __tablename__ = 'user'

    ROLE_USER = Role.USER.value
    ROLE_ADMIN = Role.ADMIN.value

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode(60), unique=True, nullable=False)
    role = db.Column(
        'role',
        Enum(Role),
        nullable=False,
        default=Role.USER, )
    password_hash = db.Column('password_hash', db.Unicode(128), nullable=False)
    password_revision = db.Column(
        'password_revision', db.SmallInteger, default=0, nullable=False)
    creation_time = db.Column('creation_time', db.DateTime, nullable=False)
    last_login_time = db.Column('last_login_time', db.DateTime)
    version = db.Column('version', db.Integer, default=1, nullable=False)


class UserToken(db.Model):  # pylint: disable=too-few-public-methods
    """Represents a token used alongside a user to authenticate operations."""

    __tablename__ = 'user_token'

    user_token_id = db.Column('id', db.Integer, primary_key=True)
    user_id = db.Column(
        'user_id',
        db.Integer,
        db.ForeignKey('user.id', ondelete='CASCADE'),
        nullable=False,
        index=True)
    token = db.Column('token', db.Unicode(36), nullable=False)
    note = db.Column('note', db.Unicode(128), nullable=True)
    enabled = db.Column('enabled', db.Boolean, nullable=False, default=True)
    expiration_time = db.Column('expiration_time', db.DateTime, nullable=True)
    creation_time = db.Column('creation_time', db.DateTime, nullable=False)
    last_edit_time = db.Column('last_edit_time', db.DateTime)
    last_usage_time = db.Column('last_usage_time', db.DateTime)
    version = db.Column('version', db.Integer, default=1, nullable=False)
