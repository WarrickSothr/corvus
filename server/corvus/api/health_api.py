"""Endpoint to expose the health of the application."""
from flask import Blueprint

from corvus.api.decorators import return_json
from corvus.api.model import APIResponse, APIMessage

HEALTH_BLUEPRINT = Blueprint(
    name='health', import_name=__name__, url_prefix='/health')


@HEALTH_BLUEPRINT.route('', methods=['GET'])
@return_json
def get_health() -> APIResponse:
    """
    Retrieve the health for the service.

    :return:
    """
    return APIResponse(
        APIMessage(True, 'Service is healthy'),
        200
    )
