"""Error definitions for Corvus."""
from typing import Dict

from werkzeug.exceptions import HTTPException

from corvus.api.decorators import return_json
from corvus.api.model import APIResponse, APIMessage


class BaseError(Exception):
    """Corvus Base Error Class (5xx errors)."""

    def __init__(
            self,
            message: str = 'Unknown error',
            status_code: int = 500,
            extra_fields: Dict[str, str] = None) -> None:
        """Populate The Error Definition."""
        super().__init__(message)
        self.message = message
        self.status_code = status_code
        self.extra_fields = extra_fields

    def to_dict(self) -> dict:
        """Serialize an error message to return."""
        return {
            'message': self.message,
            'status_code': self.status_code
        }


class ClientError(BaseError):
    """Corvus errors where the client is wrong (4xx errors)."""

    def __init__(self,
                 message: str = 'Unknown client error',
                 status_code: int = 400,
                 extra_fields: Dict[str, str] = None) -> None:
        """Init for client originated errors."""
        super().__init__(message, status_code, extra_fields)


class ValidationError(ClientError):
    """Corvus Validation Error."""


@return_json
def handle_corvus_404_error(exception: HTTPException) -> APIResponse:
    """Error handler for 404 Corvus errors."""
    return APIResponse(
        payload=APIMessage(False, 'Not Found'),
        status=exception.code if exception.code is not None else 404)


@return_json
def handle_corvus_base_error(error: BaseError) -> APIResponse:
    """Error handler for basic Corvus raised errors."""
    return APIResponse(payload=error, status=error.status_code)
