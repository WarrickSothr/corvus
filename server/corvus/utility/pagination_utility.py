"""Pagination utility functions."""
from typing import Tuple

from werkzeug.datastructures import MultiDict

from corvus import errors


def get_pagination_params(request_args: MultiDict) -> Tuple[int, int]:
    """Get page and perPage request parameters."""
    page = request_args.get('page', 1)
    per_page = request_args.get('perPage', 20)
    try:
        return int(page), int(per_page)
    except ValueError:
        raise errors.ClientError(
            ' '.join([
                'Invalid pagination parameters:',
                'page={}',
                'perPage={}']).format(page, per_page))
