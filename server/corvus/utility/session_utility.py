"""Session specific utilities."""
from typing import Any

from flask.sessions import SecureCookieSessionInterface


class DisableSessionInterface(SecureCookieSessionInterface):
    """Make sure no session data is transmitted or stored."""

    def should_set_cookie(self, app: Any, session: Any) -> bool:
        """Disable default cookie generation."""
        return False

    # pylint: disable=useless-return
    def save_session(self, app: Any, session: Any, response: Any) -> Any:
        """Prevent creating session from requests."""
        return None
