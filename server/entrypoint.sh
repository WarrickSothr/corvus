#!/usr/bin/env bash

# Migrate the Database
FLASK_APP=corvus:corvus flask db upgrade

# Make sure an administrator is registered
python manage.py user register-admin

# Start the application
gunicorn -b 0.0.0.0:8080 corvus:corvus