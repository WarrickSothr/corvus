from corvus.service.role_service import ROLES, Role


def test_role_tree_find_roles_in_hierarchy():
    roles = ROLES.find_roles_in_hierarchy(Role.USER)
    assert len(roles) == 5
    assert Role.USER in roles
    assert Role.MODERATOR in roles
    assert Role.AUDITOR in roles
    assert Role.ADMIN in roles
    assert Role.OWNER in roles

    roles = ROLES.find_roles_in_hierarchy(Role.AUDITOR)
    assert len(roles) == 3
    assert Role.AUDITOR in roles
    assert Role.ADMIN in roles
    assert Role.OWNER in roles


def test_role_tree_find_children_roles():
    roles = ROLES.find_children_roles(Role.USER)
    assert len(roles) == 2
    assert Role.USER in roles
    assert Role.ANONYMOUS in roles


def test_role_tree_find_role_key_error():
    assert len(ROLES.find_role(Role.NONE)) == 0
