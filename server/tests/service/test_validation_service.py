from corvus.model import User
from corvus.service import role_service, validation_service


def test_successful_validation():
    request_user = User()
    request_user.role = role_service.Role.USER

    user = User()
    user.role = role_service.Role.USER

    validation_result = validation_service.validate_model(request_user, user)
    assert validation_result.success


def test_failed_validation():
    request_user = User()
    request_user.role = role_service.Role.ANONYMOUS

    user = User()
    user.role = role_service.Role.USER

    validation_result = validation_service.validate_model(request_user, user)
    assert validation_result.success is False
    assert 'role' in validation_result.failed
