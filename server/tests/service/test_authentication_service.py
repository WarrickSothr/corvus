import pytest

from corvus import errors
from corvus.service import authentication_service


def test_validate_password_strength_good_password():
    proposed_good_password = 'AazZ1001'
    assert proposed_good_password == authentication_service\
        .validate_password_strength(proposed_good_password)


def test_validate_password_strength_too_short():
    proposed_good_password = 'AazZ100'
    with pytest.raises(errors.ValidationError) as error_info:
        authentication_service.validate_password_strength(
            proposed_good_password)


def test_validate_password_strength_missing_uppercase():
    proposed_good_password = 'aazz1001'
    with pytest.raises(errors.ValidationError) as error_info:
        authentication_service.validate_password_strength(
            proposed_good_password)


def test_validate_password_strength_missing_lowercase():
    proposed_good_password = 'AAZZ1001'
    with pytest.raises(errors.ValidationError) as error_info:
        authentication_service.validate_password_strength(
            proposed_good_password)


def test_validate_password_strength_missing_numbers():
    proposed_good_password = 'AAZZZZAA'
    with pytest.raises(errors.ValidationError) as error_info:
        authentication_service.validate_password_strength(
            proposed_good_password)
