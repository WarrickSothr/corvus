from mock import patch, MagicMock, Mock

from corvus.middleware.authentication_middleware import \
    authenticate_with_password, authenticate_with_token

middleware_module = 'corvus.middleware.authentication_middleware'


@patch(middleware_module + '.g')
@patch(middleware_module + '.authentication_service.is_valid_password')
@patch(middleware_module + '.user_service.find_by_name')
def test_authenticate_with_password_happy_path(
        mock_find_by_name: MagicMock,
        mock_is_valid_password: MagicMock,
        mock_g: MagicMock):
    mock_g.user = Mock()
    mock_find_by_name.return_value = Mock()
    mock_is_valid_password.return_value = True
    assert authenticate_with_password('test', 'test')
    mock_find_by_name.assert_called_once()
    mock_is_valid_password.assert_called_once()
    mock_g.user.assert_not_called()


@patch(middleware_module + '.g')
@patch(middleware_module + '.authentication_service.is_valid_password')
@patch(middleware_module + '.user_service.find_by_name')
def test_authenticate_with_password_no_user(
        mock_find_by_name: MagicMock,
        mock_is_valid_password: MagicMock,
        mock_g: MagicMock):
    mock_g.user = Mock()
    mock_find_by_name.return_value = None
    mock_is_valid_password.return_value = True
    assert not authenticate_with_password('test', 'test')
    mock_find_by_name.assert_called_once()
    mock_is_valid_password.assert_not_called()
    mock_g.user.assert_not_called()


@patch(middleware_module + '.g')
@patch(middleware_module + '.authentication_service.is_valid_password')
@patch(middleware_module + '.user_service.find_by_name')
def test_authenticate_with_password_invalid_password(
        mock_find_by_name: MagicMock,
        mock_is_valid_password: MagicMock,
        mock_g: MagicMock):
    mock_g.user = Mock()
    mock_find_by_name.return_value = Mock()
    mock_is_valid_password.return_value = False
    assert not authenticate_with_password('test', 'test')
    mock_find_by_name.assert_called_once()
    mock_is_valid_password.assert_called_once()
    mock_g.user.assert_not_called()


@patch(middleware_module + '.g')
@patch(middleware_module + '.user_token_service.is_valid_token')
@patch(middleware_module + '.user_token_service.find_by_user_and_token')
@patch(middleware_module + '.user_service.find_by_name')
def test_authenticate_with_token_happy_path(
        mock_find_by_name: MagicMock,
        mock_find_by_user_and_token: MagicMock,
        mock_is_valid_token: MagicMock,
        mock_g: MagicMock):
    mock_g.user = Mock()
    mock_find_by_name.return_value = Mock()
    mock_find_by_user_and_token.return_value = Mock()
    mock_is_valid_token.return_value = True
    assert authenticate_with_token('test', 'test')
    mock_find_by_name.assert_called_once()
    mock_find_by_user_and_token.assert_called_once()
    mock_is_valid_token.assert_called_once()
    mock_g.user.assert_not_called()


@patch(middleware_module + '.g')
@patch(middleware_module + '.user_token_service.is_valid_token')
@patch(middleware_module + '.user_token_service.find_by_user_and_token')
@patch(middleware_module + '.user_service.find_by_name')
def test_authenticate_with_token_no_user(
        mock_find_by_name: MagicMock,
        mock_find_by_user_and_token: MagicMock,
        mock_is_valid_token: MagicMock,
        mock_g: MagicMock):
    mock_g.user = Mock()
    mock_find_by_name.return_value = None
    assert not authenticate_with_token('test', 'test')
    mock_find_by_name.assert_called_once()
    mock_find_by_user_and_token.assert_not_called()
    mock_is_valid_token.assert_not_called()
    mock_g.user.assert_not_called()


@patch(middleware_module + '.g')
@patch(middleware_module + '.user_token_service.is_valid_token')
@patch(middleware_module + '.user_token_service.find_by_user_and_token')
@patch(middleware_module + '.user_service.find_by_name')
def test_authenticate_with_token_no_user_token(
        mock_find_by_name: MagicMock,
        mock_find_by_user_and_token: MagicMock,
        mock_is_valid_token: MagicMock,
        mock_g: MagicMock):
    mock_g.user = Mock()
    mock_find_by_name.return_value = Mock()
    mock_find_by_user_and_token.return_value = None
    mock_is_valid_token.return_value = False
    assert not authenticate_with_token('test', 'test')
    mock_find_by_name.assert_called_once()
    mock_find_by_user_and_token.assert_called_once()
    mock_is_valid_token.assert_called_once()
    mock_g.user.assert_not_called()


@patch(middleware_module + '.g')
@patch(middleware_module + '.user_token_service.is_valid_token')
@patch(middleware_module + '.user_token_service.find_by_user_and_token')
@patch(middleware_module + '.user_service.find_by_name')
def test_authenticate_with_token_invalid_token(
        mock_find_by_name: MagicMock,
        mock_find_by_user_and_token: MagicMock,
        mock_is_valid_token: MagicMock,
        mock_g: MagicMock):
    mock_g.user = Mock()
    mock_find_by_name.return_value = Mock()
    mock_find_by_user_and_token.return_value = Mock()
    mock_is_valid_token.return_value = False
    assert not authenticate_with_token('test', 'test')
    mock_find_by_name.assert_called_once()
    mock_find_by_user_and_token.assert_called_once()
    mock_is_valid_token.assert_called_once()
    mock_g.user.assert_not_called()
