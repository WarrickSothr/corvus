from datetime import datetime

from flask.testing import FlaskClient

from tests.conftest import AuthActions


def test_get_health_happy_path(auth: AuthActions, client: FlaskClient):
    with auth:
        auth_header = auth.get_authorization_header_token()
        result = client.get(
            '/health',
            headers={
                auth_header[0]: auth_header[1]
            })
        assert 200 == result.status_code
        assert result.json is not None
        assert result.json['message'] == 'Service is healthy'
        assert result.json['success']
