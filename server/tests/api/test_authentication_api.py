from datetime import timedelta

import rfc3339
from flask import json
from flask.testing import FlaskClient

from tests.conftest import AuthActions


def test_login_happy_path(auth: AuthActions):
    with auth as result:
        assert result.status_code == 200
        assert result.json[
                   'token'] is not None and len(result.json['token']) > 0


def test_bump_happy_path(auth: AuthActions):
    with auth:
        result = auth.bump()
        assert result.status_code == 200
        assert (result.json['lastLoginTime'] is not None
                and len(result.json['lastLoginTime']) > 0)


def test_logout_happy_path(auth: AuthActions):
    auth.login()
    result = auth.logout()
    assert result.status_code == 200
    assert result.json['success']


def test_get_tokens_no_tokens(auth: AuthActions, client: FlaskClient):
    auth_header = auth.get_authorization_header_basic()
    result = client.get(
        '/auth/token',
        headers={
            auth_header[0]: auth_header[1]
        })
    assert 404 == result.status_code
    assert result.json is not None


def test_get_tokens(auth: AuthActions, client: FlaskClient):
    with auth:
        auth_header = auth.get_authorization_header_basic()
        result = client.get(
            '/auth/token',
            headers={
                auth_header[0]: auth_header[1]
            })
        assert 200 == result.status_code
        assert result.json is not None
        assert result.json['page'] == 1
        assert result.json['lastPage'] == 1
        assert result.json['count'] == 1
        assert result.json['totalCount'] == 1
        assert result.json['items'][0]['token'] == auth.token


def test_get_nonexistant_token(auth: AuthActions, client: FlaskClient):
    auth_header = auth.get_authorization_header_basic()
    result = client.get(
        '/auth/token/not-a-token',
        headers={
            auth_header[0]: auth_header[1]
        })
    assert 404 == result.status_code
    assert result.json is not None


def test_create_get_delete_token(auth: AuthActions, client: FlaskClient):
    auth_header = auth.get_authorization_header_basic()
    result = client.post(
        '/auth/token',
        headers={
            auth_header[0]: auth_header[1],
            'Content-Type': 'application/json'
        },
        data=json.dumps({
            'note': 'test note',
            'enabled': False
        }))
    assert 200 == result.status_code
    assert result.json is not None
    assert result.json['token'] is not None
    assert result.json['note'] == 'test note'
    assert not result.json['enabled']
    assert not result.json['isValid']
    auth_token = result.json['token']
    result = client.get(
        '/auth/token/%s' % auth_token,
        headers={
            auth_header[0]: auth_header[1]
        })
    assert 200 == result.status_code
    assert result.json is not None
    assert result.json['token'] == auth_token
    result = client.delete(
        '/auth/token/%s' % auth_token,
        headers={
            auth_header[0]: auth_header[1]
        })
    assert 200 == result.status_code
    assert result.json is not None
    assert 'message' not in result.json
    assert result.json['success']


def test_create_get_delete_expired_token(
        auth: AuthActions, client: FlaskClient):
    auth_header = auth.get_authorization_header_basic()
    result = client.post(
        '/auth/token',
        headers={
            auth_header[0]: auth_header[1],
            'Content-Type': 'application/json'
        },
        data=json.dumps({
            'note': 'test note',
            'expirationTime': rfc3339.format(
                rfc3339.datetime.now() - timedelta(days=1))
        }))
    assert 200 == result.status_code
    assert result.json is not None
    assert result.json['token'] is not None
    assert result.json['note'] == 'test note'
    assert not result.json['isValid']
    auth_token = result.json['token']
    result = client.get(
        '/auth/token/%s' % auth_token,
        headers={
            auth_header[0]: auth_header[1]
        })
    assert 200 == result.status_code
    assert result.json is not None
    assert result.json['token'] == auth_token
    result = client.delete(
        '/auth/token/%s' % auth_token,
        headers={
            auth_header[0]: auth_header[1]
        })
    assert 200 == result.status_code
    assert result.json is not None
    assert 'message' not in result.json
    assert result.json['success']


def test_delete_nonexistant_token(auth: AuthActions, client: FlaskClient):
    auth_header = auth.get_authorization_header_basic()
    result = client.delete(
        '/auth/token/not-a-token',
        headers={
            auth_header[0]: auth_header[1]
        })
    assert 404 == result.status_code
    assert result.json is not None
