from typing import Any

from flask import Response, Flask

from corvus.api.decorators import return_json
from corvus.api.model import APIResponse


@return_json
def return_jsonified_result(obj: Any) -> Any:
    return obj


def test_return_json_response():
    result = return_jsonified_result(Response(status=200))
    assert isinstance(result, Response)
    assert result.status_code == 200


def test_return_json_apiresponse(app: Flask):
    with app.app_context():
        result = return_jsonified_result(APIResponse(payload={}, status=200))
        assert len(result) == 2
        assert isinstance(result[0], Response)
        assert isinstance(result[1], int)
        assert result[0].status_code == 200


def test_return_json_dict(app: Flask):
    with app.app_context():
        result = return_jsonified_result({'status': 200})
        assert isinstance(result, Response)
        assert result.status_code == 200
