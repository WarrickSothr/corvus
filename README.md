# Corvus [![pipeline status](https://gitlab.com/WarrickSothr/Corvus/badges/master/pipeline.svg)](https://gitlab.com/WarrickSothr/Corvus/commits/master)

A python flask framework and web client.

## Parts

### Server

The core API server

More information available at server/README.md

### Administration

The administration SPA.

More information available at client-admin/README.md

## Release History

## Changelog

See:
* server/CHANGELOG.md
* administration/CHANGELOG.md

## FAQ

* TODO

## Maintainers

* Drew Short <warrick(AT)sothr(DOT)com>
